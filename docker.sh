#!/bin/bash
#docker run -it -v ~/workspace/atlantis/src:/home/atlantis/src atlantis_src
docker run -d -p 3306:3306 --name mysql_atlantis -e MYSQL_ALLOW_EMPTY_PASSWORD=true -e MYSQL_DATABASE=atlantis -e MYSQL_USER=atlantis -e MYSQL_PASSWORD=atlantis -v ~/workspace/atlantis/data:/var/lib/mysql mysql:5.7
docker run -d --network host trsek/atlantis