@echo off
rem --------------------------------------------------------------------------
rem Vytvori Docker z MySQL verzie 5.7
rem DB sa cez volume pripaja z docker_mysql_57_data.zip. Uz je vyplnena datami.
rem Potom spusti app napisanu pre ubuntu z image trsek/atlantis. 
rem 
rem                                         Software (c) 2021, Zdeno Sekerak
rem --------------------------------------------------------------------------

docker run -d -p 3306:3306 --name mysql_atlantis -e MYSQL_ALLOW_EMPTY_PASSWORD=true -e MYSQL_USER=atlantis -e MYSQL_PASSWORD=atlnatis -v C:\Docker\app_data:/var/lib/mysql mysql:5.7
docker run -d --name atlantis trsek/atlantis