#!/bin/bash
docker ps -a -q --filter ancestor=ubuntu | xargs docker stop
docker ps -a -q --filter ancestor=ubuntu | xargs docker rm
docker ps -a -q -f status=exited | xargs docker rm
docker ps -q | xargs docker stop
docker ps -q | xargs docker rm
docker rm $(docker ps -aq)
docker rmi $(docker images -q -f dangling=true)
docker container prune
docker image prune