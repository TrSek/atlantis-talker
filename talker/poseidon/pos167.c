/****************************************************************************
********************       P+O+S+E+I+D+O+N       ****************************
********************                             ****************************
******************** (c) 1998 Lone Star Software ****************************
********************  (a part of Rider Software) ****************************
****************************************************************************/

/*********************************  NEWS  ***********************************
verzia 1.2d:
      -  ovlada novy prikaz ".travel <luzer>", ktory povodi luzera po celej 
         Atlantide a prasti ho do basy :>. Inak pole rooms a pocet v ROOMS. 
      -  bugfixnuty control command (~~FKK), isiel tam do minusu (bolo tam
         for 4 miesto 5!)
      -  odpazeny ten sprintf, co hadzal core na alfe (uz _totalne_ )
      -  zopar novych hlasok
      -  reaguje na snahu o kiss (... sa Ta pokusil pobozkat !)      
      -  reaguje na hug (*** Pozdravuje ta ...! ;-) ***)
      -  osetrene floodovanie v .tell-och + bugfixnuty firstword()	
      -  a uz je stvrt na dve v noci, tak idem chraapat, lebo mi uz sibe :)

verzia 1.3a
      -  odpazene warningy z lokalnych premennych (malo za nasledok vznik par
         globalnych, nie je to ktovieco, ale na drhu stranu by to mohlo vysvet-
         lit niektore "neidentifikovatelne" pady Posa - ked sa tam nejaky 
         pointer scwokol, tak to kludne mohlo drbnut...)
      -  nova funkcia curword() UPLNE nahradila bukovu firstword() + nove 
         moznosti. Ovldadanie: curword(*char nejaka_ta_veta, int pozicia_slova)
         Pozicia slova sa udava od jednotky (blby perzitok zo ZX Spectra :>>).
         V pripade debilneho hodnoty vrati "*". Kedze prakticky robila TO ISTE
         co firstword (ale aj s inymi ako prvym slovom), tak som to nahradil...
      -  osetreny bug, ze ked mal niekto v .desc meno "master-a", tak pos z 
         toho zblbol a zdravil dotycneho master-a
      -  osetreny bug s .travel (flood "Pokusil!") spinavou fintou (.time) ;-)
      -  quoli znizenie floodu (teleport..) sa pri travel Pos odpazi na Nebesa
      -  floodkill uz neshoutuje, ale iba wizshoutuje

verzia 1.3d
      -  naucil sa konecne riadne zdravit (nejako to totiz blblo)
      -  KONECNE novy prikaz ".forward" (bez parametra) - presmeruje vsetky
         hlasky okrem svojich tellov osobe, ktora ho o forward poziadala.
         Dufam, ze to neni zabugovane (klop-klop), papral som sa s tym asi 
         pat hodin :-)
      -  Dalsi novy prikaz (ja som ale usilovny ;-): ".repeat <kolko> <co>".
         Nadherne na floodovanie (niekoho...). Strasne neprijemne zneuzitelne,
         ak to niekto prezenie, OKAMZITE ho odpisem zo zoznamu!!! Len si
         predstavte, co by spravilo take:
          .tel poseidon ~~FKK .repeat 100000 .shout ~~LBApocalypse TODAY!!!
         Pricom Pos by tento prikaz v pohode zvladol!!! Brrr... Pre istotu
         (nechceny flood) sa Pos pri repeate VZDY odpazi na nebesa....
      -  Vzhladom na to, ze je o 2 minuty polnoc, idem uz spat :> 

verzia 1.3e
      -  Zajtra mam skusku z MO, takze ziadne superzmeny :-)
      -  Malilinkata uprava na repeat - sekundovy sleep pri repeat vacsom ako
         5 (to uz totiz ide o zjavne "stvanie" niekoho - a to je lepsie pomale
      -  Bugfixnuty "Tak si radsej precitam board" (cez emote :>)
      -  Ak sa dostanem zasa do trablov (ked mi bude switorit a demotnem sa) ze
         ma nejaky dobrak (caf Prizo!) demotne na slejva a pridem o tell, uz
         za z toho dostanem - pouzitim SOS!
      -  funkcia "travel (luzername)" a funkcia "opakuj (char co[], int kolko)"
         sa osamostantili (Indipendence Day ;-)
      -  Novy prikaz (tak PREDSA!) ".zbavma <luzername>", zatial celkovo 7 nie
         zrovna najmilsich sposobov, ako zabavit cloveka, ktore zufalo milujeme
         a nevieme, ako mu to povedat (kill, jail, travel, hugflood etc..) ;-)
         Prizo by mohol vymysliet par novych zlomyselnosti, ma na to talent :)
      -  Dalsi novy prikaz (ja som neni normalny!) ".help" -> vypise vsetky
         prikazy, co Pos pozna (resp. ktore su definovane v PRIKAZY :>). Uz 
         som mal v tom totiz bordel :-)))))
      -  Do paze, ale ja este FAKT musim ist pozriet ten Medzinarodny Obchod :(

verzia 1.4a
      - Bugfixnuty .forward pri .revwiz a NAJMA pri odhlaseni sa dotycneho 
        individua bez toho, aby vypol forward (dost velky bug, swina!)
      - Nastalo obdobie PROHIBICIE!!! Nie, klud, chlastat sa moze veselo dalej,
        prohibicia sa tyka shoutu! Definovane MAXSHOUT a NOSHOUT, pricom 
        MAXSHOUT je maximalny pocet shoutov jednej osoby v poli prohibit o
        dlzke NOSHOUT, takze vlastne pocas jedneho NOSHOUTU moze dotycny
        lamer shoutnut iba MAXSHOUT krat.. potom letiii.... A buffer sa
        necisti, takze ak bude pokracovat, ma ihned smolu.. holt, tazky zivot.
        Snaha o okludnene "rozshoutovanej" Atlantidy nekonvencnymi sposobmi.
        (Nechcel bych sa furt prihlasovat, teda... ;-)
      - Pred poslednym moznym shoutom to este varuje... Nech to niesu jatky...
        A nekilluje (zbytocny flood) ale iba execne na quit ;-) Tiez potesi ;-)
      - Je tam este jeden mensi bug, ci skqor nedokonalost... ak bude niekto
        shoutovat prave vtedy ked jeho meno bude opustat tabulku, moze sa stat,
        ze ho to bude varovat viac krat, ba dokonca ze ho to nevyhodi... avsak 
        to by nesmel shoutovat nikto iny... Pravdepodobnost je to mala, ale je.
        Zial, s tym sa neda (zatial) nic robit, avsak vhodne rozmery NOSHOUTU a
        MAXSHOUT by to mali spravit... (akurat ich urcit, sax...)
      - MAXFLOOD nastaveny na 3, lebo ked niekto nieco opakuje _viac_ ako dva 
        razy, tak to uz _je_ flood a nie error.. Takze uplne tymto odstavime 
        akukolvek snahu o floodovanie.. (viz. niektori dobraci, ktori sa 
        odhlasuju shoutom "CHCEM IST PREC" ci take cosi 5x po sebe... Fcil 
        staci 3x a to sa da zniest ;-)
      - NOSHOUT je zatial 50 a MAXSHOUT 8, realne hodnoty ukaze az prax :(
        Na "tichom" talker sa to skratka vyskusat neda, na to treba Kurta, 
        Bekekeho, Adyna, Sana a ostatne "kvietky"... Potom sa ukaze, ako tie
        cislicka pomenit tak, aby bol _konecne_ klud, ale aby to zasa nejako
        neobmedzovalo poctivych ludi, ktori chcu pozdravit etc...
      - Spravil som si zadne dvierka, ak ma zasa Pasmo demotne na slejva, 
        zabasne a navyse muzzlne... uz za z toho dostanem (kam sa cpe nejaky
        Copperfield ;-)). Na poziadanie urobim aj dalsim, zatial sa mi nechcelo
      - Jedna malicka undocumented feature ciste pre osobnu potrebu ;->>
      - Quoli zneuzivaniu bugu v echo je sanca osoby ziadajucej Posa o promote
        uz 4:1, snad to znizi jatky.... Echo, secho a pecho treba zakazat pre
        posa priamo v zdrojaku Atlantisu, inak je to f pazi... :(      
      - Bugfixnuta dost skareda chyba pri ovladani Poseidona, na ktoru
        trochu zabudol Buko pri aplikovani funkcie "ovlada". Nastastie vdaka
        dvojitej ochrane (meno+znak) nedoslo k zneuziti posa a prejavilo sa 
        to iba ako malicky bug pri tellovani riadiaceho znaku.     
      - Bugfixnuta DALSIA chyba v .forward (do paze, tych tam je...) a to v
        revtel. Ak tam mal totiz riadiace znaky, tak sa pri tom zacyklil a 
        robil desny bordel. Zvlastne, ze sa na to zatial neprislo. Nie je
        lahke to osetrit (chcelo by to hladat zaciatok a koniec revtel 
        buffera etc..), rozhodne mi to nestalo za to, aby som sa s tym
        babral nejak viac.. tak som to skratka zatrhol. Obdobne revshout
        (plnil by zbytocne shout buffer), review a revwiz. Jedine uzitocne
        bolo iba to posledne, ale takto aspon nebude z casu na cas wizshoutovat
        blbosti ;-)) Sice som si narobil zbytocnu robotu v prvom bode (co som
        bugfixoval wizshout), ale takto je to aspon koser ;-)
      - Opraveny preklep v helpe (repeat, uz ma to srdilo...)
      - No, myslim, ze som to s tym pisanim dneska trochu prehnal takze na dnes
        stacilo. Vlastne som chcel povodne iba pridat tu prohibiciu a opravit
        forward pri odhlasovani... holt, ked sa objavili dalsie chyby, co som 
        mal s nimi robit?! ;-)) Jak tak ziram, kolko som toho sem napisal, idem
        radsej napisate nejake scificko ;-)))
      - Este jedna undocumented featrure, ale ja sa uz polepsim ;-)))

verzia 1.4b
      - Co bude robit lamer, ked mu zakazu shout? Shout-emotovat! Bugfixnute...
      - A co spravi, ak nemoze ani semotovat? Bude sechovat... Fixed... ;-)
        (ja som ale swina, co? ;-[]) Potom uz dufam bude klud...

verzia 1.4c
      - No, ze niesom az taka swina a hlavne po Spakkyho PPckach shout protec-
        tion mierne strtilo vyznam. Ale kedze sa mi to nechcelo odpazit uplne,
        tak som vyrobil novy prikaz ".prohibit", ktory zapina/vypina shout
        protection ;-)) <ake zvrrrhle, zee? :>>
      - Par little bugfixov
      - Osetrene milove odpazeovanie poseidona (este sa to da oblafnut echom,
        ale nic nie je dokonale ;-)

verzia 1.4c+
      - Maly bugfix v .shout  "....blabla tells you: blabla...", kde sa Pos 
        zbytocne ozyval...
      - Dorobena little uprava pre volbu Miss atlantis ;-)
      - Mala uprava v undocumented feature 1 - bolo to desne neuprimne :(
      - Dorobene posluchanie na .quit (radsej...)
      - Bugfixnuty wizshout s meno "poseidon"
      - Zmenene heslo (Milo vie preco....)
      - uf2 dorobena o wrap lines 

verzia 1.4d
      - Zrusene undocumented features (okrem "budicka", ale tusim blbne :()
      - Pridana grupa "privileged" - premena privils, ktorej ucastnici mozu 
        pouzivat niektore poseidonove funkcie (zatial .kill, .pictel, .move,
        .travel a .bomb).
      - Uplne prerobene detekovanie a "zdravenie" ludi, jak masters tak aj
        privileged + mensi bugfix koli .pecho pos SIGN ON .... (ale iba MALY -
        uplne sa to zrejme osetrit neda)
      - Predchadazajuca uprava si vyziadala par sleepov, uvidime... 

verzia 1.5
      - Verzii 1.4x bolo uz straasne vela, tak ideme na 1.5 :))
      - Bugfixnuty VELKY bug pri telle prazdneho riadaceho znaku (core dumped:)
      - Maly bugfix v "budicku", lebo to pozehnane blblo :)
      - Uprava reakcie podla charakteru nadavky (thnx. to Milo)
      - Pridane reakcie na par novych slov
      - Maly bugfix (resp. uprava) vo funkcii opakuj(), nejako to blblo...
      - Uprava v detekcii floodu pri .cls a pod. (nech nekilluje hviezdicku:)
      - Prikaz ".jail" a ".unjail" pre grupu privileged (ked uz mali travel...)
      - Riadiaci string bez parametra vypise help (lepsie ako core dumped)
      - Uplne odstranena funkcia hangaround() - aj tak som ju nevyuzival...
      - Novy prikaz ".alltopic <aky topic chceme>" - nastavi dany topic vo
        vsetkych miestnostiach definovanych v rooms[]

verzia 1.5a
      - Definitivne ZRUSENE vsetky "undocumented features"
      - Potlaceny pozdravny shout na Jamesona <ved ja viem preco>
      - Povolene .revwiz (zasa!!)
      - Reakcie na wizshouty iba ak nie je zapnuty forward (pre revwiz)
      - Dorobeny novy prikaz ".fmode" s parametrom 0-3 (zatial), pricom:
                    .fmode 0  -  default (forwarduje VSETKO co vidi)
                    .fmode 1  -  forwarduje IBA telly co dostava
                    .fmode 2  -  forwarduje IBA wizshouty co dostava
                    .fmode 3  -  forwarduje telly a wizshouty
        Prikaz bez parametra vypise prave nastaveny mod. POZOR! .fmode iba
        NASTAVUJE mod forwardovania, avsak neinicializuje forward! Vyhoda je
        v tom, ze mozno lubovolne menit mod pocas forwardu, ale mozno sa s tym
        raz pohram a dodam to do .forward [x]
      - Poupravovany forward (aby sa to znasalo s .fmode)
      - Este by sa s tym chcelo pohrat, ale som out of time :(

verzia 1.6 (final release)
      - Tolko podstatnych zmien, ze som prestal so seriou 1.5x a dokonca
        som to hodil pod hlavicku Lone Star Software :)
      - Uplne ZRUSENY Jameson a ine zivly (uz je to ozaj "cisty" zdrojak)
      - Zrusene vsetky slipy (teda sleep-y), pricom pribudol iba jeden do
        sendmud() -> mierna uprava v repeat(), uz netestuje 50 opakovani.
      - Vyhodil som sice documented, ale nie zrovna fair vsuvku, ktora ma
        promotovala, ked som dal SOS alebo unmuzzla, ked som zmenil desc.
        To druhe aj tak uz bolo na nic, lebo v Atlantide sa pri muzzle uz
        desc. neda menit. Aby sa vsak nepovedalo, ze iba rusim, spravil som
        to tak, ze HOCIKTO kto je zo skupiny "ovlada" moze v pripade nudze
        pouzit .sos a Poseidon ho promotne. Tymto sa Poseidon v1.6 stava
        UPLNE CISTYM kodom bez akychkolvek zakompilovanych vyhod pre jedno-
        tlivca, bez vseliakych "undocumented features", cheatov a podobnych
        kravin... Pekne, nie?
      - Osetrene zdravenie pri "Emerging" (quoli Bukovi, ktoreho odmietal
        pozdravit a Buko sa za to na mna zlostil ;))
      - Fixnuty VELKY bug s preplnenim output buffera pri generaci odpovede.
        Pri dlhsich vetach to vyhadzovalo nadherne core dumped... teraz by to
        uz malo byt okej (bwt, pekny navod ako zhodit Milovho E-Maila, staci sa
        trochu ozrat a spytat sa ho "co co coc co co co co co co co co co" a 
        na 90% spadne s core dumped.... Doteraz padal aj Pos, ale uz je fixed).
      - Vyhodena funkcia swap_file(). Vobec nefim, na co tu bola, sluzil na
        prehodenie pointrov dvoch otvorenych suborov (ked sa tmp. subor
        meni na skutocny). Nicim nebola nikdy volana, fakt nechapem, co tu
        oxidovala...
      - Takisto vyhodena funkcia makestring(), ktora sice tusim mala alokovat
        nejaku pamat na tvorbu stringu, ale takisto ako swap_file() bola uplne
        nevyuzita a ani nemyslim, ze by sa mi na nieco hodila... Kazdopadne,
        nebola odnikial volana a tak isla prec.
      - Uplne NOVY sposob bootovania robota, teraz sa to forkuje na pozadie
      - Nove parametre (k -dt pribudli -nl):
           n: vypina forkovanie (No fork)
           l: zapina logovanie do suboru (Logging)
        Prepinac 'n' vlastne nahradza 'emulaciu' povodneho sposobu bootovania,
        tj. vsetky vypisy idu na obrazovku ako pred tym, vhodne pre spustanie
        pod screenom a pod. Sluzi skor na testovanie ako na "serioznu" pracu.
        Vyhody forku su zrejme -> ziaden screen, ziaden nohup, skratka sa to
        pusti a bezi to akoby sa ani nechumelilo na pozadi a basta. Prepinac
        'l' je obdobou noveho prikazu ".log" (viz. dalej) pre logovanie
        vsetkeho, co Poseidon vidi (vratane connectu). Okrem toho, do logu
        sa samozrejme nezapisuje identifikacka (POS:). 
      - Pribudla globalna premenna logging, definicia LOGNAME, upravene boli
        vystupy z stderr do suboru, na obrazovku iba pri noforku
      - Novy prikaz .log, ktory je alternativou prepinaca -l. Vyhodou je, ze
        ho mozno lubovolne aktivizovat alebo deaktivizovat. Zatial loguje
        UPLNE VSETKO co vidi, ma to svoje vyhody (univerzalnost) aj nevyhody
        (dlzka logu). Prave koli tomuto som este dopisal prikaz .logmode
        (viz. nizsie), umoznujuci logovat iba podla urcitej vzorky...
      - Drobne upravy koli predmetom: obcas, ked sa nudi a nikto mu nic 
        zaujimaveho nehovori, tak si stvori nejaky-ten predmet... Inak pozor,
        treba mu nastavit .ignfun (uklada sa do userfajlu, takze no problem),
        aby to nezblblo... (ludia mu davaju bomby a podobne potesenicko...)
      - Popisanych par helpov (najma k #define), aby bolo trochu jasnejsie
        co na co je :)
      - Par drobnych bugfixov a hlavne KONECNE odstranene vsetky liuxove
        (na Alfe neviem, ale v linuxe uz fakt vsetky) warningy, dokonca aj
        tie, co boli uz v povodnej "kostre"!!! Takze pri gcc -wall je to uz
        cistucke ako lalia.. :)
      - Pridane dalsie hlasky do "Poseidonovych shoutov". Tieto shouty sa uz
        stavaju klasikou Atlantidy a minule ma par ludi zvozilo za to, ze ich
        uz vsetky poznaju. Okej, ako chceli: teraz je tam celkovo 40 (!$#!)
        roznych driistov, dufam ze vystacia dost dlho :)
      - Prikaz .zdrav na "oblafnutie" ludi, ktori furt chcu, aby ich Pos
        zdravil. Teraz ich moze zdravit - pravda, iba po najblizsi reboot.
        Zdravi ich rovnako ako grupu "privileged", ale dotycna osoba nema
        nijake prava... Zatial iba pre jednu osobu.
      - Ako vhodny sa mi javil prikaz .logmode. Bez parametra ukaze momentalny
        mod (masku alebo vzorku) logovania, inak umoznuje nastavit tuto masku.
        Prikaz je "case insensitive", takze s velkymi/malymi pismenami si
        hlavu lamat netreba. Napr. ".logmode you sax" bude logovat vsetky vety,
        v ktorych su slova "you sax". UPOZORNENIE: prikaz .logmode NEZAPINA
        logovanie, iba nastavuje jeho mod. Logovanie treba aktivizovat zvlast
        prikazom .log!
      - Fixnuty bug v prikaze .alltopic pre jednopismenovy topic, pridana
        funkcia char *wordtoend(string,slovo);, ktora vracia slova zo stringu
        "string" od slova cislo "slovo" do konca stringu (je to vlastne iba
        upraveny curword(), ale ten v spojeni so while...!"*"... strcat()...
        pekne blbol, tak som na to spravil zvlast funkciu).

verzia 1.6s (Secured Release)
      - Uplne zrusene funkcie waitfor() a robot(). Z povodneho zdrojaku tymto
        ostavaju iba najnizsie funkcie, pracujuce so socketmi...
      - Zmenene heslo (preco asi....)
      - Odstranena funkcia crash_robot (nebola vyuzita)
      - Velke upravy v main (v casti byvalej waitfor()), detekcia session
        swap, osetrene hlasenia pri pade robota, vyhodene neefektivne casti
        kodu, mierne optimalizacie, vyhodenych par zbytocnych premennych...
      - quit_robot doplneny o zapis udalosti pri zapnutom logovani, pripadne
        o zapis do obrazovky + driver pre AutoReconnect
      - Desiatky dalsich bugfixov a mensich uprav
      - Zrejme zavlecene nove bugy - zmenilo sa totiz naozaj VELA
      - Dobudovany AutoReconnect - v pripade, ze Poseidon sa odhlasi alebo ze
        spadne... Nepracuje v pripade, ze spadne Atlantis (nie ze by to bol
        problem dopisat, ale furt by volal sam seba a to je zbytocne, ved nie
        je problem ho nahadzovat spolu s Atlantidou).
      - V pripade SessionSwap sa Poseidon OKAMZITE znovu prihlasi, cim zasa
        swapne session... trochu o drzku, ale potencialny (uspesny) hacker by
        mal len cca 3 sekundy na to, aby stihol nieco napachat...
      - Proces uz nezostava "visiet" ako sa to stavalo prakticky od zaciatku
      - VAROVANIE: Poseidona sa NESMIE spustit viac ako jeden proces! Inak
        by to znamenalo desnu pohromu, tie procesy by sa bili o to, ktory bude
        aktivny a Poseidon by sa stale reloginoval! Zatial neviem o jednoduchom
        sposobe, ako to kontrolovat, tak si skratka treba davat poriadne bacha!
        Ono sa to totiz potom neda ani tak lahko killnut, furt to meni PID (ved
        to vola samo seba a tak... A zabanovat Poseidona je tiez dost tazko, o
        minlogine nehovorim). Kazdpadne "killall -9 pos.exe" to isti :)
      - Klasicky bug: .echo This was online chat.... atd, a Poseidon sa spola-
        hlivo odhlasil. V tejto verzi je to uz (KONECNE!) definitivne fixnute a
        kontrola disconnectu prebieha na uplne INOM principe... (ako pri swap).
      - Poseidon je teraz zabezpeceny dokonca aj proti SIGTERMU!! T.j. ani s
        jeho skillovanim to nebude take jednoduche :) Jedine cez -9, inak sa
        skratka iba rebootne. Vyhoda je v tom, ze ak Atlantis medzitym drisne
        (napr. pri reboote alphy ked sa posiela sigterm), skratka sa uz druhy
        krat nenahlasi a quitne sa so statusom OK, pricom sa korektne uzavrie
        aj logfile... Diabolsky premyslene, co? :)
      - Tradicne ".go pos" po logine Poseidona bolo vypnute (kedze teraz sa
        clovek prihlasi tam, odkial sa odlasil je to nanic)
      - Kedze Poseidon sa stal prakticky neznicitelnou bestiou (nejde kill, 
        nejde quit...), pribudol novy prikaz ".exitpos" s parametrami "reboot"
        alebo "shutdown". Co robia je myslim jasne. Reboot je dobry na
        nahodenie novej verzie, shutdown pre "nepredvidane okolnosti" (napr.
        prave vtedy, ak nejaky dobrak spusti proces s poseidonom viac krat a
        nastane chaos). Tymto by sa mala tato dost osemetna zalezitost
        vyriesit....        
      - Kedze som furt miesto .tel pos verzia pisal iba .pos verzia, spravil
        som prikaz ".verzia", ktory vrati prave beziacu verziu Poseidona...
      - Na zaver som este vychytal par bugov, ktore som sem zavliekol pri
        implementovani tothotuvsetkeho co je hore :)

verzia 1.666
      - Par uprav pre Portalis (chce to vsak _ovela_viac_)
      - Zmena hesla: NUnutBFv60llw
      - Zopar novych hlasok (Where do you want to go today?:)
      
verzia 1.67 (S)
      - loguje VSETKY stringy co dostane ... len tak sa moze zistit
        preco pada ...
        (a aj ine veci, ked napr. ludia s nim blbnu)
*****************************************************************************/

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <varargs.h>
#include <signal.h>
#include <unistd.h>

#define VERSION "Poseidon, v1.67 (c)1998 by Lone Star Software (Rider) & Buko"

#define BUFFSIZ 16384  /* velkost buffera pre i/o */
#define MAXSTRING 2048 /* maximalna dlzka stringu */
#define MAXWORD 1024   /* maximalna dlzka slova   */
#define CAKAJ 600      /* pocet sekund, co sa Pos moze "nudit" */
#define NOSHOUT 10     /* v podstate dlzka shout bufferu */
#define MAXSHOUT 8     /* max. vyskyt shoutov v buffere */
#define MAXFLOOD 3     /* prevencia floodu */

#define LOGFILE "poseidon.log"   /* nazov suboru s logom */
#define LOG_ALL "everything.log" /* (S) */

#define MUDHOST "localhost"
#define MUDPORT 7000

#define MYNAME "poseidon"
#define MYPASS "posi.nosi"

#define MASTERS 5 /* Komu bude Poseidon sluzit... ;) */
char *masters[] = {"Rider", "Buko", "Prizo", "Spartakus", "Psi"};

#define PRIVILS 0 /* A kto ho este moze pouzivat...  */
char *privils[] = {"Pasmo", "Milo"};

/* toto bude help, obsahujuci vsetky interne commandy Poseidona */
#define PRIKAZY "Mozne prikazy: .travel <luzer>; .repeat <kolkokrat> <co>; .zbavma <luzer>; .forward; .help; .prohibit; alltopic <akytopic>; .fmode [mode]; .log; .logmode [maska|cancel]; .zdrav [meno|cancel]; .exitpos [reboot|shutdown]; .verzia"

/* miestnosti pre nastavovanie topicu vo funkcii alltopic */
#define ROOMS 16
char *rooms[] = {"pristav", "namestie", "krcma", "afroditin", "poseidonov", "academia", "arena", "svatyna", "amfiteater", "trhovisko", "ostrov", "mesto", "vzdusny", "vrchol", "luka", "udolie"};

/* globalna premenna na bordeltext :) */
char text[MAXSTRING];

#define ctrl(C) ((C)&037)
#define strcomp(A, B) (!strcmp((A), (B)))
#define streq(A, B) (strstr((A), (B)))
#define READ 0
#define WRITE 1

int flood = 0;                   /* Na detekciu floodu 	 */
char fbuff[MAXSTRING];           /* Flood buffer 	 */
char cword[MAXSTRING];           /* Pre funkciu curword() */
char blboutput[MAXSTRING];       /* blbec() 	 	 */
char nemoutput[MAXSTRING];       /* nemo() 		 */
char lowoutput[MAXSTRING];       /* strlow()		 */
char prohibit[NOSHOUT][MAXWORD]; /* pre D.O.S.  */
char prohon = 0;                 /* Prohibit on/off   */
char zdrav[MAXWORD] = "";        /* Koho bude zdravit */
char logmode[MAXSTRING] = "";    /* Mod logovania     */
char PROGRAMNAME[MAXWORD];       /* Nie cez #define, ale argv[0] */

int fmud, tmud;
int debug = 0;
int testing = 0;
int forking = 1;
int logging = 0;
int dead = 0;
int hanging = 0;
int forward = -1;
int formode = 0;

FILE *logfile;

char *getmud();
char *nemo();
char *strlow();
char *curword();
char *wordtoend();
int ovlada(char *);
int privil(char *);
void travel(char[]);
void opakuj(char[], long);
int connectmud();
void readmsg(char *);
void sendmud(char *);
int quit_robot(int);
int charsavail(int);
void log_input(char *); /*(S)*/

void log_input(str) /*(S)*/
    char *str;
{
   FILE *fp;

   if ((fp = fopen(LOG_ALL, "a")) == NULL)
      return;
   fputs(str, fp);
   fprintf(fp, "\n");
   fclose(fp);
}

/**************************************************************** 
 * Main - boot az hlavna programova slucka...
 ****************************************************************/

int main(argc, argv) int argc;
char *argv[];

{
   char *msg;
   char luzername[MAXWORD], shbuff[MAXSTRING];
   int poz, count, ourtime;
   void sighandle();
   time_t cas;

   strcpy(PROGRAMNAME, argv[0]);

   fprintf(stderr, "\n\n************* Booting GodBot *************\n%s\n\n(c) LONE STAR     __/\\__\n       SOFTWARE   >_><_<\nAll rights reserved \\/\n\n", VERSION);

   /* Get the options from the command line */
   while (--argc > 0 && (*++argv)[0] == '-')
   {
      while (*++(*argv))
      {
         switch (**argv)
         {
         case 'd':
            debug++;
            fprintf(stderr, "Debug mode ENABLED...\n");
            break;
         case 't':
            testing++;
            fprintf(stderr, "Test mode ENABLED...\n");
            break;
         case 'n':
            forking--;
            fprintf(stderr, "Forking mode DISABLED...\n");
            break;
         case 'l':
            logging++;
            fprintf(stderr, "Logging mode ENABLED...\n");
            break;
         default:
            fprintf(stderr, "\nUsage: pos.exe [-dtlf]\n\nNote: -d: Debug\n      -t: Test\n      -n: not forking\n      -l: Log to file (poslog.log)\n");
            fprintf(stderr, "BOOT failed (invalid commands)\n");
            exit(1);
         }
      }
   }

   if (testing)
   {
      tmud = 1;
      fmud = 0;
   }
   else
   {
      tmud = fmud = connectmud();
   }

   if (tmud < 0)
   {
      fprintf(stderr, "FAILED\n");
      exit(-1);
   }

   else
      fprintf(stderr, "OK\n");

   if (forking)
      switch (fork())
      {
      case -1:
      {
         fprintf(stderr, "ERROR: fork failure\n");
         quit_robot(1);
      }; /* fork failure */
      case 0:
         break; /* child continues */
      default:
         sleep(1);
         exit(0); /* parent dies */
      }

   if (logging)
   {
      fprintf(stderr, "Opening log file...");
      if ((logfile = fopen(LOGFILE, "at")) == NULL)
      {
         printf("FAILED!\n");
         logging = 0;
      }
      else
         fprintf(stderr, "OK\n");
   }

   if (logging)
      fprintf(logfile, "*** Booted successfully with PID %5d ***\n\n", getpid());

   /* Signal trapping */
   signal(SIGTERM, sighandle);

   fprintf(stderr, "*** Booted successfully with PID %5d ***\n\n", getpid());

   for (poz = 0; poz < NOSHOUT; poz++)
      strcpy(prohibit[poz], "nobody");

   sleep(1);
   sendmud(MYNAME);
   sendmud(MYPASS);

   /****************************************************************************
 *                Well Done: now MAIN PROGRAM LOOP 
 ****************************************************************************/

   while (1)
   {
      ourtime = CAKAJ + time(&cas);
      while (ourtime > time(&cas))
      {
         if ((msg = getmud()) == 0)
         {
            if (logging && forking)
               fprintf(logfile, "***** Connection lost\n");
            else if (forking == 0)
               fprintf(stderr, "***** Connection lost\n");
            quit_robot(0);
         }
         log_input(msg); /*(S)*/
         readmsg(msg);

         /* Prohibicia - nech zije klub DOS! (Dobrovolny Odporcovia Shoutu!) */

         if ((((!strcmp(curword(msg, 2), "shouts:") || !strcmp(curword(msg, 2), "shout-echuje:")) && isupper(msg[0]) && strcmp(curword(msg, 1), "You")) || (!strcmp(curword(msg, 1), "!!") && isupper(curword(msg, 2)[0]))) && prohon)

         {
            count = 1;

            if (!strcmp(curword(msg, 1), "!!"))
               strcpy(luzername, curword(msg, 2));
            else
               strcpy(luzername, curword(msg, 1));

            for (poz = 0; poz < NOSHOUT - 1; poz++)
            {
               strcpy(prohibit[poz], prohibit[poz + 1]);
               if (!strcmp(luzername, prohibit[poz]))
                  count++;
            }
            strcpy(prohibit[NOSHOUT - 1], luzername);
            if (count == MAXSHOUT - 1)
            {
               sprintf(shbuff, ".tell %s Na tvojom mieste by som ~LI~OLOKAMZITE~RS prestal shoutovat...", luzername);
               sendmud(shbuff);
            }
            else if (count >= MAXSHOUT)
            {
               sprintf(shbuff, ".tell %s Varoval som ta... BYEEEEE!!!!", luzername);
               sendmud(shbuff);
               sprintf(shbuff, ".exec %s .quit", luzername);
               sendmud(shbuff);
            }
         }

         /* Koniec prohibicie, fsak sme ludia... eee... teda roboti!!!!      */

         if ((!strcmp(curword(msg, 2), "asks") || !strcmp(curword(msg, 2), "tells")) && !strcmp(curword(msg, 3), "you:") && isupper(msg[0]))
         {
            if (!strstr(curword(msg, 1), "You"))
               sendmud(nemo(msg));
         }
         /* Spracovanie TELLOV a podobne -> dolezite! */
         else if (streq(curword(msg, 1), "POZOR:") && streq(curword(msg, 5), "SOS") && ovlada(curword(msg, 2)))
         {
            sprintf(text, ".promote %s VYNIMOCNY STAV!", curword(msg, 2));
            sendmud(text);
         }
         else if (streq(msg, " shouts:") && streq(strlow(msg), "poseidon"))
            switch (rand() % 45)
            {
            case 0:
               sendmud(".shout Haha, mrrtvola sa ozvala!");
               break;
            case 1:
               sendmud(".shout Co hovori?! Uraza nas?!~LB");
               break;
            case 2:
               sendmud(".shout Tiicho tam v podpalubiii!");
               break;
            case 3:
               sendmud(".shout Drrrz hubu a krok, okeej?!");
               break;
            case 4:
               sendmud(".shout Neber meno bozie nadarmo!!!");
               break;
            case 5:
               sendmud(".shout Radsej ma neprovokuj!");
               break;
            case 6:
               sendmud(".shout MFP");
               break;
            case 7:
               sendmud(".shout Ked sa nahnevam, budem zly!");
               break;
            case 8:
               sendmud(".shout ;-)");
               break;
            case 9:
               sendmud(".shout Jee, to je od teba mile.");
               break;
            case 10:
               sendmud(".shout Zase videli Yetiho...");
               break;
            case 11:
               sendmud(".shout Chod sa radsej hrat Minesweeper!");
               break;
            case 12:
               sendmud(".shout To hovor konovi, ten ma vacsiu hlavu!");
               break;
            case 13:
               sendmud(".shout Spriateleny god vzdy ti prijde vhod!");
               break;
            case 14:
               sendmud(".fortune all");
               break;
            case 15:
               sendmud(".shout Hrali ste uz noveho Dooma???");
               break;
            case 16:
               sendmud(".shout Co? Ja? Nieee!!!");
               break;
            case 17:
               sendmud(".shout Clovece, kde ty chodis na tie sprostosti?");
               break;
            case 18:
               sendmud(".shout Jaak jaaa to maaam f paziii......");
               break;
            case 19:
               sendmud(".shout Tiez tak neznasate matiku???");
               break;
            case 20:
               sendmud(".shout Nevsimaj si to, HAL, su to iba ludia...");
               break;
            case 21:
               sendmud(".shout Zaraza ma,ako moze byt taky inteligentny clovek taky SPROSTY!");
               break;
            case 22:
               sendmud(".shout Ocuvaj, co keby som ti tak vybanoval site, he?");
               break;
            case 23:
               sendmud(".shout Som umela inteligencia, ale to neznamena, ze som odfarbena blondynka!");
               break;
            case 24:
               sendmud(".shout Sklapni ty rura z megafonu a nevrieskaj tu!");
               break;
            case 25:
               sendmud(".shout Jaaaaaak skuuuuuuusaaas??? Cooooo skuuuuuusaaaas???");
               break;
            case 26:
               sendmud(".shout Neondiaj ma lebo ta zaondiam a budes poondiaty!");
               break;
            case 27:
               sendmud(".shout Jak to se mnou mluvis, ty PRDE?!?!");
               break;
            case 28:
               sendmud(".shout Rideer!! Bukooo! Prizoo!! Spakkyy!! Zasa ma ohovaaraju!!!");
               break;
            case 29:
               sendmud(".shout Ticho tam pod papierom lebo splachnem!!!");
               break;
            case 30:
               sendmud(".shout Joj ty kykymor, ogrgel akyysi!! Fuj ti!");
               break;
            case 31:
               sendmud(".shout Mupy Mup!!! ~OL~FY~BR~  ~BK");
               break;
            case 32:
               sendmud(".shout Nemtudom, ja Slovak!!! ~BW ~BB ~BR ~BK");
               break;
            case 33:
               sendmud(".shout L~al~a ho papl~uha, ogrcal mi kapce!");
               break;
            case 34:
               sendmud(".shout Spravaj sa uctivo ked sa rozpravas s GODom!!!");
               break;
            case 35:
               sendmud(".shout Za Narod, Za Atlantis, Za GODov!!!");
               break;
            case 36:
               sendmud(".shout To nie je vydlicka, ty truba, to je TROJZUBEC!");
               break;
            case 37:
               sendmud(".shout Poculi ste, ze v pristave videli Lochnesku??");
               break;
            case 38:
               sendmud(".shout Atlantida je taka kraaasna.... Hlavne ked som tu ja!");
               break;
            case 39:
               sendmud(".shout Ktora blondynka si to so mnou minule dohodla rande???");
               break;
            case 40:
               sendmud(".shout Where do you want to go today?");
               break;
            case 41:
               sendmud(".shout Taaaaaaaaaaaaaaaaaak zle!!!\n");
               break;
            case 42:
               sendmud(".shout Ocuj, nechces sa radsej hrat na piesocku?");
               break;
            case 43:
               sendmud(".shout Nuz, keby som bol blby ako ty, tak ti na to odpoviem...");
               break;
            case 44:
               sendmud(".shout Nooo a cooo!!!! :>>>");
               break;
            }

         else if (forward == -1 && !strcmp(curword(msg, 2), "wizshouts:") && streq(strlow(msg), "poseidon"))
            switch (rand() % 3)
            {
            case 0:
               sendmud(".wizshout Som iba poctivy robot, nic viac. ;)");
               break;
            case 1:
               sendmud(".wizshout Ale no tak, wizardi! ;)");
               break;
            case 2:
               sendmud(".wizshout Si robte prcu, tam na vas skocim...!!!");
               break;
            }

         else if (streq(msg, "(to Poseidon):") || (streq(strlow(msg), "poseidon") && streq(msg, "says:")))
            switch (rand() % 12)
            {
            case 0:
               sendmud(".say Ocuj, radsej mi to tellni...");
               break;
            case 1:
               sendmud(".fortune all");
               break;
            case 2:
               sendmud(".say Co furt mate proti mojej malickosti!?");
               break;
            case 3:
               sendmud(".say Hm, co takto .kill?!");
               break;
            case 4:
               sendmud(".sing This happened once before, when I came to your door, no reply...");
               break;
            case 5:
               sendmud(".cow");
               break;
            case 6:
               sendmud(".cow");
               break;
            case 7:
               sendmud(".fortune all");
               break;
            case 8:
               sendmud(".say Nemam cas.");
               break;
            case 9:
               sendmud(".say Bodaj by to bola pravda!");
               break;
            case 10:
               sendmud(".say Hmmm, radsej si precitam board...");
               sendmud(".emote reads the message board.");
               break;
            case 11:
               sendmud(".say A o inom nevies...?");
               break;
            }
         else if (streq(strlow(msg), "poseidon") && streq(msg, "asks:"))
            switch (rand() % 5)
            {
            case 0:
               sendmud(".say Mna sa pytas??");
               break;
            case 1:
               sendmud(".cow");
               break;
            case 2:
               sendmud(".say Co furt mate proti mojej malickosti!?");
               break;
            case 3:
               sendmud(".say To sa pytaj niekoho ineho!");
               break;
            case 4:
               sendmud(".say A ty?");
               break;
            }
         else if (streq(strlow(msg), "poseidon") && streq(msg, "exclaims:"))
            switch (rand() % 5)
            {
            case 0:
               sendmud(".fortune all");
               break;
            case 1:
               sendmud(".cow");
               break;
            case 2:
               sendmud(".say Co furt mate proti mojej malickosti!?");
               break;
            case 3:
               sendmud(".say Neprovokuj ma!");
               break;
            case 4:
               sendmud(".say Mam pouzit prikaz .kill?");
               break;
            }

         else if (streq(strlow(msg), "atlanti") && streq(msg, "shouts:") && strcmp(curword(msg, 2), "wizshouts:"))
            switch (rand() % 7)
            {
            case 0:
               sendmud(".shout Atlantidovica? Kedy? Kde? Ako? S kym?");
               break;
            case 1:
               sendmud(".shout Pozri si stranku Atlantisu: ~OL~FGhttp://www.atlantis.sk~RS !");
               break;
            case 2:
               sendmud(".shout Co sa vam nepaci na Atlantise?");
               break;
            case 3:
               sendmud(".shout Atlantis RuLeZ!");
               break;
            case 4:
               sendmud(".shout Ak mas problem s Atlantisom, napis na ~OLtalker@atlantis.sk~RS");
               break;
            case 5:
               sendmud(".sing Zem pradaaaavnych sllnc....");
               break;
            case 6:
               sendmud(".shout Hm, Atlantis... Co ty na to, Platon?");
               break;
            }

         /* MISS ATLANTIS - po akcii odpazit :>>>  Odtialto............ 
         else if (streq(strlow(msg),"miss") && streq(msg,"shouts:") && strcmp(curword(msg,2),"wizshouts:"))
             switch(rand()%7)
             {
             case 0: sendmud(".shout Najkrajsia z najkrajsich - MISS ATLANTIS!");
                     break;
             case 1: sendmud(".shout Informacie o Miss Atlantis: www.atlantis.sk/miss");
                     break;
             case 2: sendmud(".shout Chcete sa prihlasit do sutaz o Miss Atlantis? Poslite e-mail na talker!");
                     break;
             case 3: sendmud("# si volbu Miss Atlantis URCITE nenecha ujst!!!");
                     break;
             case 4: sendmud(".shout Hezke holky: http://www.atlantis.sk/miss !!!");
                     break;
             case 5: sendmud("# si skoro oci vyo...ee.. kamery vykameroval pri pohlade na kandidadky o MISS!");
                     break;
             case 6: sendmud(".shout Tie najsamsuper koc~cky z Atlantidy najdete na http://www.atlantis.sk/miss!!!");
                     break;
             }
      ...................az potialto!!!!!!!! */

         else if (strstr(msg, " sa Ta pokusila pobozkat !"))
            switch (rand() % 2)
            {
            case 1:
               strcpy(text, ".kiss ");
               strcat(text, curword(msg, 1));
               sendmud(text);
               sendmud(".sing Love me tender, love me true...");
               break;
            default:
               strcpy(text, ".tell ");
               strcat(text, curword(msg, 1));
               strcat(text, " Och, teraz nie, laska chce svoj cas ;-))");
               sendmud(text);
               break;
            }

         else if (strstr(msg, "*--* H U G *--* Pozdravuje Ta") && strstr(msg, "! *--* H U G *--*"))
         {
            for (poz = 18; msg[poz] != '\0' && msg[poz] != '!'; poz++)
               luzername[poz - 18] = msg[poz];
            luzername[poz - 18] = '\0';
            strcpy(text, ".hug ");
            strcat(text, luzername);
            sendmud(text);
         }
         else if ((streq(msg, "SIGN OFF: ") || streq(msg, "Vanishing in")))
         {
            if (forward != -1)
               if (strstr(msg, masters[forward]))
                  forward = -1;
         }
         else if ((streq(msg, "SIGN ON: ") || streq(msg, "Emerging from the deeps of cyberspace: ")))
         {
            strcpy(text, "");
            if (privil(curword(msg, 3)) || !strcmp(zdrav, curword(msg, 3)) || ovlada(curword(msg, 3)) || ovlada(curword(msg, 7)))
            {
               if (privil(curword(msg, 3)) || !strcmp(zdrav, curword(msg, 3)) || ovlada(curword(msg, 3)))
                  strcpy(luzername, curword(msg, 3));
               else
                  strcpy(luzername, curword(msg, 7));
               if (ovlada(luzername))
               {
                  sprintf(text, ".tell %s Vitaj, moj Pane! Cakam na Tvoje rozkazy...", luzername);
                  sendmud(text);
                  strcpy(text, "");
                  /*
                        switch (rand()%5)
                          {
                          case 0: sprintf(text,".shout Vitaaaaj v Atlantide, %s!",luzername);
                                  break;
                          case 1: sprintf(text,".shout Ahooj %s!",luzername);
                                  break;
                          case 2: sprintf(text,".shout Nazdarrr %s!",luzername);
                                  break;
                          case 3: sprintf(text,".shout Zdravim ta %s!",luzername);
                                  break;
                          case 4: sprintf(text,".shout Welcome %s!",luzername);
                                  break;
                          }
                         */
               }
               else
               {
                  sprintf(text, ".tell %s Vitam ta v Atlantide a zelam ti krasny den!", luzername);
                  sendmud(text);
                  strcpy(text, "");
                  /*
                        switch (rand()%5)
                          {
                          case 0: sprintf(text,".shout Ahojky %s!",luzername);
                                  break;
                          case 1: sprintf(text,".shout Vitam ta, %s!",luzername);
                                  break;
                          case 2: sprintf(text,".shout Jeej, %s, vitaj!",luzername);
                                  break;
                          case 3: sprintf(text,".shout Vitaj v Atlantide %s!",luzername);
                                  break;
                          case 4: sprintf(text,".shout %s %s %s! Caf!",luzername,luzername, luzername);
                                  break;
                          }
                        */
               }
            }
            if (strcmp(text, ""))
               sendmud(text);
         }
      }

      switch (rand() % 13)
      {
      case 0:
         sendmud(".go pos");
         break;
      case 1:
         sendmud(".go nam");
         break;
      case 2:
         sendmud(".go nebesa");
         break;
      case 3:
         sendmud(".go pristav");
         break;
      case 4:
         sendmud(".shout Zivot je kraaaasny.... teda ako kde..");
         break;
      case 5:
         sendmud(".shout Ludia, mate f pazi?! Ja hej...");
         break;
      case 6:
         sendmud(".say Vy ste puci, ja som puk, ale o tom ani muk!");
         break;
      case 7:
         sendmud(".sing She loves you yeah, yeah, yeah!!!");
         break;
      case 8:
         sendmud(".say Jeej, v pondelok bude Mulder a Scullyova!!!");
         break;
      case 9:
         sendmud(".cow");
         break;
      case 10:
         sendmud(".shout Mas problem? Spytaj sa Poseidona!");
         break;
      case 11:
         sendmud(".fortune all");
         break;
      case 12:
         sendmud(".think Aj tak je najlepsia hra Indiana Jones And The Fate Of Atlantis!");
         break;
      }
   }
}

/**************************************************************** 
 * readmsg: Handle messages + detect flood ! (Buko) 
 ****************************************************************/

void readmsg(msg) char *msg;
{
   char luzername[MAXWORD], forbuf[MAXSTRING];
   char *p;

   p = msg;
   while (*p != '\0')
   {
      if (*p == '%')
         *p = ' ';
      p++;
   }

   if (logging && forking)
   {
      if (strlen(logmode))
      {
         if (strstr(strlow(msg), logmode))
            fprintf(logfile, "%s\n", msg);
      }
      else
         fprintf(logfile, "%s\n", msg);
   }
   else if (forking == 0)
      fprintf(stderr, "POS: %s\n", msg);

   if (strcmp(curword(msg, 1), "You") && forward != -1 && strlen(msg) && (((strstr(msg, " tells you: ") || strstr(msg, " asks you: ")) && formode == 1) || (formode == 0) || (strstr(msg, " wizshouts: ") && formode == 2) || ((strstr(msg, " tells you: ") || strstr(msg, " asks you: ") || strstr(msg, " wizshouts: ")) && formode == 3)))
   {
      sprintf(forbuf, ".tell %s ~FT~OL%s~RS~FW", masters[forward], msg);
      sendmud(forbuf);
   }

   if (!flood)
      strcpy(fbuff, msg);
   if (!strstr(curword(msg, 1), "You") && strlen(msg))
      if (!strcmp(fbuff, msg))
      {
         flood++;
         if (flood == MAXFLOOD)
         {
            strcpy(luzername, curword(msg, 1));
            sprintf(text, ".wizshout ~OLFLOOD DETECTED: ~FR%s~FW ! (killed)", luzername);
            sendmud(text);
            sprintf(text, ".kill %s", luzername);
            sendmud(text);
            flood = 0;
         }
      }
      else
         flood = 0;
}

/********************************************************************
 * quit_robot: We are exiting.  Write out any long term memory first. 
 *             flag: 0 = reboot, 1 = shutdown
 ********************************************************************/

int quit_robot(flag) int flag;
{

   if (logging && forking && flag == 0)
      fprintf(logfile, "***** Crash! Trying to reboot...\n");
   else if (forking == 0 && flag == 0)
      fprintf(stderr, "***** Crash! Trying to reboot...\n");

   close(tmud);
   close(fmud);

   if (logging)
      fclose(logfile);
   /* Write out any memory files needed */

   if (flag == 0)
      system(PROGRAMNAME);
   /* Run copy of itself -> prevencia proti session swap! */

   exit(0);
}

/**************************************************************** 
 * connectmud: Open the Atlantis socket 
 ****************************************************************/

int connectmud()
{
   struct sockaddr_in sin;
   struct hostent *hp;
   int fd;

   fprintf(stderr, "Connecting to Atlantis...");

   bzero((char *)&sin, sizeof(sin));

   sin.sin_port = htons(MUDPORT);

   if ((hp = gethostbyname(MUDHOST)) == 0)
      return (-1);

   bcopy(hp->h_addr, (char *)&sin.sin_addr, hp->h_length);
   sin.sin_family = hp->h_addrtype;

   fd = socket(AF_INET, SOCK_STREAM, 0);
   if (fd < 0)
      return -1;

   if (connect(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
      return -1;

   return fd;
}

/**************************************************************** 
 * sendmud: Send a command to the Atlantis process 
 ****************************************************************/
/* new zmena - odpazene parametre pre sendmud.. <alfa...> */

void sendmud(fmt) char *fmt;
{
   int len;
   char buf[BUFSIZ];

   sprintf(buf, fmt);
   strcat(buf, "\n");
   len = strlen(buf);

   if (debug && logging && forking)
      fprintf(logfile, "\nSend: %s\n", buf);
   else if (debug && forking == 0)
      fprintf(stderr, "\nSend: %s\n", buf);

   if (write(tmud, buf, len) != len)
   {
      if (logging && forking)
         fprintf(logfile, "Write failed: %s", buf);
      else if (forking == 0)
         fprintf(stderr, "Write failed: %s", buf);
      quit_robot(0);
   }
   sleep(1);
}

/**************************************************************** 
 * getmud: Read one line from Atlantis 
 ****************************************************************/

char *getmud()
{
   int len;
   static char buf[BUFSIZ], rbuf[4];
   register char *s = buf;

   /* No input waiting */
   if (!charsavail(fmud))
      return (NULL);

   /* Read one line, save printing chars only */
   while ((len = read(fmud, rbuf, 1)) > 0)
   {
      if (*rbuf == '\n')
         break;
      if (isprint(*rbuf))
         *s++ = *rbuf;
   }
   *s = '\0';

   /* Check for error */
   if (len < 0)
   {
      if (logging && forking)
         fprintf(logfile, "Error %d reading from mud\n", len);
      else if (forking == 0)
         fprintf(stderr, "Error %d reading from mud\n", len);
      quit_robot(0);
   }
   return (s = buf);
}

/***************************************************************** 
 * charsavail: check for input available from 'fd' 
 *****************************************************************/

int charsavail(fd) int fd;
{
   long n;
   int retc;

   if ((retc = ioctl(fd, FIONREAD, &n)) != 0)
   {
      if (logging && forking)
         fprintf(logfile, "Ioctl returns %d, n=%ld.\n", retc, n);
      else if (forking == 0)
         fprintf(stderr, "Ioctl returns %d, n=%ld.\n", retc, n);
      quit_robot(0);
   }

   return ((int)n);
}

/*********************************************************
  sighandle: Vychytava sigterm, pripadne dalsie signaly
 *********************************************************/

void sighandle()
{
   if (logging && forking)
      fprintf(logfile, "***** Sigterm recieved!\n");
   else if (forking == 0)
      fprintf(stderr, "***** Sigterm recieved!\n");
   quit_robot(0);
}

/************ "Inteligencia" ;) *************/

char *chword(word) char *word;
{
   if (!strcmp(word, "si"))
      return ("som");
   if (!strcmp(word, "som"))
      return ("si");
   if (!strcmp(word, "ty"))
      return ("ja");
   if (!strcmp(word, "ja"))
      return ("ty");
   if (!strcmp(word, "my"))
      return ("vy");
   if (!strcmp(word, "vy"))
      return ("my");
   if (!strcmp(word, "tebe"))
      return ("mne");
   if (!strcmp(word, "mne"))
      return ("tebe");
   if (!strcmp(word, "moj"))
      return ("tvoj");
   if (!strcmp(word, "tvoj"))
      return ("moj");
   if (!strcmp(word, "ma"))
      return ("ta");
   if (!strcmp(word, "ta"))
      return ("ma");
   if (!strcmp(word, "budes"))
      return ("budem");
   if (!strcmp(word, "budem"))
      return ("budes");
   if (!strcmp(word, "mas"))
      return ("mam");
   if (!strcmp(word, "mam"))
      return ("mas");
   if (!strcmp(word, "ano"))
      return ("nie");
   if (!strcmp(word, "nie"))
      return ("ano");
   if (!strcmp(word, "povedz"))
      return ("nepoviem");
   if (!strcmp(word, "napis"))
      return ("nenapisem");
   if (!strcmp(word, "posli"))
      return ("neposlem");
   if (!strcmp(word, "mi"))
      return ("ti");

   if (word[strlen(word) - 1] == 's' && strcmp(word, "atlantis") && strcmp(word, "les") && strcmp(word, "ples") && strcmp(word, "dnes"))
      if (word[strlen(word) - 2] == 'e' || word[strlen(word) - 2] == 'i' || word[strlen(word) - 2] == 'a')
         word[strlen(word) - 1] = 'm';

   if (!strcmp(word, "ahoj") || streq(word, "caf") || !strcmp(word, "nazdar") || !strcmp(word, "servus") || !strcmp(word, "cau") || !strcmp(word, "hi"))
      switch (rand() % 6)
      {
      case 0:
         word = "nazdar";
         break;
      case 1:
         word = "ahoj";
         break;
      case 2:
         word = "cafff";
         break;
      case 3:
         word = "vitaj";
         break;
      case 4:
         word = "zdravim ta";
         break;
      case 5:
         word = "hi";
         break;
      }

   if (!strcmp(word, "kolko"))
      switch (rand() % 6)
      {
      case 0:
         word = "vela";
         break;
      case 1:
         word = "malo";
         break;
      case 2:
         word = "asi 243";
         break;
      case 3:
         word = "ani neviem kolko";
         break;
      case 4:
         word = "presne 876 a pol";
         break;
      case 5:
         word = "plna vana";
         break;
      }

   if (!strcmp(word, "koho"))
      switch (rand() % 6)
      {
      case 0:
         word = "niekoho konkretneho";
         break;
      case 1:
         word = "takeho debilneho";
         break;
      case 2:
         word = "asi Buka";
         break;
      case 3:
         word = "urcite Ridera";
         break;
      case 4:
         word = "pravdepodobne Spartaka";
         break;
      case 5:
         word = "zrejme Priza";
         break;
      }

   if (!strcmp(word, "kto"))
      switch (rand() % 5)
      {
      case 0:
         word = "niekto konkretny";
         break;
      case 1:
         word = "nikto";
         break;
      case 2:
         word = "asi kapitan Nemo";
         break;
      case 3:
         word = "nejaky dilino";
         break;
      case 4:
         word = "taky lamer z EU";
         break;
      case 5:
         word = "niekto blizsie neurceny";
         break;
      case 6:
         word = "Rider alebo Buko";
         break;
      case 7:
         word = "Spakky ci Prizo";
         break;
      }

   if (!strcmp(word, "kde"))
      switch (rand() % 5)
      {
      case 0:
         word = "f pazi";
         break;
      case 1:
         word = "na Marse";
         break;
      case 2:
         word = "v jame";
         break;
      case 3:
         word = "v zadku";
         break;
      case 4:
         word = "na neznamom mieste";
         break;
      }

   if (!strcmp(word, "kedy"))
      switch (rand() % 5)
      {
      case 0:
         word = "dnes";
         break;
      case 1:
         word = "prave teraz";
         break;
      case 2:
         word = "zrejme dnes";
         break;
      case 3:
         word = "v nejakom neurcitom case";
         break;
      case 4:
         word = "teraz";
         break;
      }

   if (!strcmp(word, "ako"))
      switch (rand() % 5)
      {
      case 0:
         word = "po slovensky";
         break;
      case 1:
         word = "po rusky";
         break;
      case 2:
         word = "normalne";
         break;
      case 3:
         word = "urcite nejako";
         break;
      case 4:
         word = "istym sposobom";
         break;
      }

   if (!strcmp(word, "kam"))
      switch (rand() % 5)
      {
      case 0:
         word = "do paze";
         break;
      case 1:
         word = "na Mars";
         break;
      case 2:
         word = "do Ruska";
         break;
      case 3:
         word = "do jamy levovej";
         break;
      case 4:
         word = "pod stol";
         break;
      }

   if (!strcmp(word, "co"))
      switch (rand() % 5)
      {
      case 0:
         word = "nieco";
         break;
      case 1:
         word = "hnile jablko";
         break;
      case 2:
         word = "prasknuty monitor";
         break;
      case 3:
         word = "nieco konkretne";
         break;
      case 4:
         word = "asi UFO";
         break;
      }

   if (!strcmp(word, "naco"))
      switch (rand() % 3)
      {
      case 0:
         word = "nanic";
         break;
      case 1:
         word = "len tak";
         break;
      case 2:
         word = "na prd";
         break;
      }

   if (!strcmp(word, "ake"))
      switch (rand() % 5)
      {
      case 0:
         word = "debilneho";
         break;
      case 1:
         word = "hnile";
         break;
      case 2:
         word = "ruske";
         break;
      case 3:
         word = "male";
         break;
      case 4:
         word = "usate";
         break;
      }

   if (!strcmp(word, "aky"))
      switch (rand() % 5)
      {
      case 0:
         word = "zeleny";
         break;
      case 1:
         word = "ruzovy";
         break;
      case 2:
         word = "modry";
         break;
      case 3:
         word = "gulaty";
         break;
      case 4:
         word = "velky";
         break;
      }

   if (!strcmp(word, "aka"))
      switch (rand() % 5)
      {
      case 0:
         word = "kockata";
         break;
      case 1:
         word = "zelena";
         break;
      case 2:
         word = "pekna";
         break;
      case 3:
         word = "sarmantna";
         break;
      case 4:
         word = "blba";
         break;
      }

   if (!strcmp(word, "koho"))
      switch (rand() % 5)
      {
      case 0:
         word = "takeho lamera";
         break;
      case 1:
         word = "prezidenta";
         break;
      case 2:
         word = "Buka alebo Priza";
         break;
      case 3:
         word = "Spartaka alebo Ridera";
         break;
      case 4:
         word = "tchora";
         break;
      }

   if (!strcmp(word, "komu"))
      switch (rand() % 5)
      {
      case 0:
         word = "staremu ujovi";
         break;
      case 1:
         word = "upratovacke";
         break;
      case 2:
         word = "zubarke";
         break;
      case 3:
         word = "ufonom";
         break;
      case 4:
         word = "styrom slonom";
         break;
      }

   if (!strcmp(word, "kym"))
      switch (rand() % 5)
      {
      case 0:
         word = "pocitacom";
         break;
      case 1:
         word = "mackom Uskom";
         break;
      case 2:
         word = "Winnetuom";
         break;
      case 3:
         word = "luzerom";
         break;
      case 4:
         word = "Meciarom";
         break;
      }

   if (!strcmp(word, "coho"))
      switch (rand() % 5)
      {
      case 0:
         word = "slona";
         break;
      case 1:
         word = "ziletky";
         break;
      case 2:
         word = "monitora";
         break;
      case 3:
         word = "telefonu";
         break;
      case 4:
         word = "debilneho";
         break;
      }

   if (!strcmp(word, "comu"))
      switch (rand() % 5)
      {
      case 0:
         word = "lampe";
         break;
      case 1:
         word = "babke";
         break;
      case 2:
         word = "diskboxu";
         break;
      case 3:
         word = "pocitacu";
         break;
      case 4:
         word = "Beatlesu";
         break;
      }

   if (!strcmp(word, "cim"))
      switch (rand() % 5)
      {
      case 0:
         word = "autobusom";
         break;
      case 1:
         word = "ponorkou";
         break;
      case 2:
         word = "telefonom";
         break;
      case 3:
         word = "niecim neurcitym";
         break;
      case 4:
         word = "vreckovkou";
         break;
      }

   if (!strcmp(word, "ktory"))
      switch (rand() % 5)
      {
      case 0:
         word = "ten treti";
         break;
      case 1:
         word = "taky suchy";
         break;
      case 2:
         word = "niktory";
         break;
      case 3:
         word = "ten zafarbeny";
         break;
      case 4:
         word = "ten zly";
         break;
      }

   if (!strcmp(word, "ktora"))
      switch (rand() % 5)
      {
      case 0:
         word = "tretia zlava";
         break;
      case 1:
         word = "ta blondinka";
         break;
      case 2:
         word = "ta prezidentova";
         break;
      case 3:
         word = "ta skulava";
         break;
      case 4:
         word = "taka pekna";
         break;
      }

   if (!strcmp(word, "ake"))
      switch (rand() % 5)
      {
      case 0:
         word = "take";
         break;
      case 1:
         word = "to srandovne";
         break;
      case 2:
         word = "take debilneho";
         break;
      case 3:
         word = "to ufonske";
         break;
      case 4:
         word = "take hranate";
         break;
      }

   if (!strcmp(word, "akeho"))
      switch (rand() % 5)
      {
      case 0:
         word = "takeho";
         break;
      case 1:
         word = "toho debilneho";
         break;
      case 2:
         word = "lamerskeho";
         break;
      case 3:
         word = "kockateho";
         break;
      case 4:
         word = "krpateho";
         break;
      }

   if (!strcmp(word, "kolkeho") && (!strcmp(word, "kolkateho")))
      switch (rand() % 5)
      {
      case 0:
         word = "sestnasteho";
         break;
      case 1:
         word = "dvadsiateho deviateho";
         break;
      case 2:
         word = "druheho";
         break;
      case 3:
         word = "11-teho";
         break;
      case 4:
         word = "niektoreho";
         break;
      }

   if (!strcmp(word, "akym"))
      switch (rand() % 5)
      {
      case 0:
         word = "takym zvrhlym";
         break;
      case 1:
         word = "nemoznym";
         break;
      case 2:
         word = "zelenym";
         break;
      case 3:
         word = "mierne priblblym";
         break;
      case 4:
         word = "nijakym";
         break;
      }

   if (!strcmp(word, "odkedy"))
      switch (rand() % 5)
      {
      case 0:
         word = "odvcera";
         break;
      case 1:
         word = "odjakziva";
         break;
      case 2:
         word = "od cias kolumba";
         break;
      case 3:
         word = "odkedy je svet svetom";
         break;
      case 4:
         word = "odvtedy jak padla linka";
         break;
      }

   return (word);
}

/*********** blbec() *************/
char *blbec(instring, user) char *instring, user[20];
{
   char string[MAXSTRING], word[MAXWORD];
   int poz = 0, outpoz = 0;
   blboutput[0] = '\0';

   /* Tolowerne a odstrany debilneho medzery... */
   while (*(instring + poz) != '\0')
   {
      if ((*(instring + poz) != '?') && (!isspace(*(instring + poz)) || (isspace(*(instring + poz)) && !isspace(*(instring + poz - 1)))))
         (string[outpoz++]) = tolower(*(instring + poz));
      poz++;
   }

   string[outpoz] = '\0';

   /* Zoberie meno usera z instringu
strcpy(user,curword(instring,1)); */

   /* Fcilek ideme brat slowa...*/

   poz = 0;
   blboutput[0] = '\0';

   if (strstr(string, "preco"))
      switch (rand() % 7)
      {
      case 0:
         strcpy(blboutput, "Neviem...");
         break;
      case 1:
         strcpy(blboutput, "Lebo banan nema zips...");
         break;
      case 2:
         strcpy(blboutput, "Pre stare vreco a novu zaplatu...");
         break;
      case 3:
         strcpy(blboutput, "Pre nic.");
         break;
      case 4:
         strcpy(blboutput, "Preco nie?");
         break;
      case 5:
         strcpy(blboutput, "Pre blizsie neurcene dovody.");
         break;
      case 6:
         strcpy(blboutput, "Aby bola sranda.");
         break;
      }

   else if (strstr(string, "coze"))
      switch (rand() % 6)
      {
      case 0:
         strcpy(blboutput, "Ale nic.");
         break;
      case 1:
         strcpy(blboutput, "Mas davat pozor.");
         break;
      case 2:
         strcpy(blboutput, "Ale, to si len tak brblem...");
         break;
      case 3:
         strcpy(blboutput, "Nic, zabudni na to...");
         break;
      case 4:
         strcpy(blboutput, "Hm, nic...");
         break;
      case 5:
         strcpy(blboutput, "Ale, nieco sa ti snivalo");
         break;
      }

   else if (strstr(string, "verzia") || strstr(string, "version") || strstr(string, "program") || (strstr(string, "kto") && (strstr(string, "tvoril") || strstr(string, "napisal") || strstr(string, "vymyslel"))))
      strcpy(blboutput, VERSION);
   else if (streq(string, "kto") && streq(string, "si"))
      strcpy(blboutput, "Som Poseidon, inteligentny robot, a davam tu na toto pozor. A ty si kto?");
   else if (strstr(string, "debiln"))
      strcpy(blboutput, "Mas pravdu, je to debilneho.");
   else if (strstr(string, "english") || strstr(string, "speak") || strstr(string, "language"))
      strcpy(blboutput, "I don't speak English. I speak just Slovak.");
   else if (strstr(string, "IQ") || strstr(string, "inteligen"))
      strcpy(blboutput, "Narazas na moju inteligenciu? Urcite je vyssia ako tvoja!");
   else if (strstr(string, "kekes"))
      strcpy(blboutput, "Na kekesov je expert Spartakus, ved sa ho spytaj! :)");
   else if (strstr(string, "demot") || strstr(string, "suicide") || strstr(string, "zalar"))
      strcpy(blboutput, "S tymto sa musis obratit na niektoreho z GODov!");
   else if (strstr(string, "pomoc") || strstr(string, "help") || strstr(string, "pomoz"))
      strcpy(blboutput, "Skus napisat .pomoc alebo .help, precitaj si .rules a .faq");
   else if (strstr(string, "meciar") || strstr(string, "kovac") || strstr(string, "prezident") || strstr(string, "premier"))
      strcpy(blboutput, "O politike sa s tebou nebavim, ja som slusny robot!");
   else if (strstr(string, "skap") || strstr(string, "umri") || strstr(string, "zomri"))
      switch (rand() % 4)
      {
      case 0:
         strcpy(blboutput, "A neskapem!");
         break;
      case 1:
         strcpy(blboutput, "Az po tebe!");
         break;
      case 2:
         strcpy(blboutput, "Skap ty!");
         break;
      case 3:
         strcpy(blboutput, "To by sa ti hodilo, co?");
         break;
      }
   else if (strstr(string, "hovno") || strstr(string, "zmrd") || strstr(string, "debil") || strstr(string, "kurva") || strstr(string, "keket") || strstr(string, "blbec") || strstr(string, "fuck") ||
            strstr(string, "kokot") || strstr(string, "jeb") || strstr(string, "idiot") || strstr(string, "chuj") || strstr(string, "kreten") || strstr(string, "hovado") || strstr(string, " pica"))
   {
      if (!strcmp(curword(string, 1), "ty") || !strcmp(curword(string, 1), "si"))
         switch (rand() % 6)
         {
         case 0:
            strcpy(blboutput, "AJ TY!");
            break;
         case 1:
            strcpy(blboutput, "Urazat sa nenecham!");
            break;
         case 2:
            strcpy(blboutput, "My sme spolu husi nepasli!");
            break;
         case 3:
            strcpy(blboutput, "Nadavaj svojej babicke a nie mne!");
            break;
         case 4:
            strcpy(blboutput, "Sklapni a daj bacha, lebo budes mat trable!");
            break;
         case 5:
            strcpy(blboutput, "Hej, mam ti ukazat co je to NAZURENY GOD?!");
            break;
         }
      else if (!strcmp(curword(string, 1), "ja"))
         switch (rand() % 6)
         {
         case 0:
            strcpy(blboutput, "No, neber to v zlom, ale to si!");
            break;
         case 1:
            strcpy(blboutput, "Trochu sebakritky by nezaskodilo!");
            break;
         case 2:
            strcpy(blboutput, "Ale nie, ty si iba taka truba...");
            break;
         case 3:
            strcpy(blboutput, "No a co ked si?!");
            break;
         case 4:
            strcpy(blboutput, "AJ TY?!");
            break;
         case 5:
            strcpy(blboutput, "No, to je aj problem viacerych politikov...");
            break;
         }
      else
         switch (rand() % 6)
         {
         case 0:
            strcpy(blboutput, "Davaj si pozor na jazyk!");
            break;
         case 1:
            strcpy(blboutput, "Clovece, krot sa!");
            break;
         case 2:
            strcpy(blboutput, "S prasatami sa nerozpravam...");
            break;
         case 3:
            strcpy(blboutput, "Trhni si lavou zadnou...");
            break;
         case 4:
            strcpy(blboutput, "Hm, tvoja slovna zasoba ma este medzery...");
            break;
         case 5:
            strcpy(blboutput, "Mas IQ ako hoblik, ked takto rozpravas?");
            break;
         }

      sprintf(text, ".wiz WIZARD %s je zmrd, lebo mi stale nadava! ;)", user);
      sendmud(text);
   }
   else if (strstr(string, "zaco") || strstr(string, "za co") || strstr(string, "za kolko"))
      switch (rand() % 7)
      {
      case 0:
         strcpy(blboutput, "Za fajku mocky.");
         break;
      case 1:
         strcpy(blboutput, "Za deravy gros.");
         break;
      case 2:
         strcpy(blboutput, "Za dieru z kolaca...");
         break;
      case 3:
         strcpy(blboutput, "Len tak...");
         break;
      case 4:
         strcpy(blboutput, "Zadarmo.");
         break;
      case 5:
         strcpy(blboutput, "Za macku vo vreci");
         break;
      case 6:
         strcpy(blboutput, "Za 500 korun");
         break;
      }

   else
   {

      while (string[poz] != '\0')
      {
         for (outpoz = 0; (string[poz] != ' ') && (string[poz] != '\0'); word[outpoz++] = string[poz++])
            ;
         word[outpoz] = '\0';

         /* fcul mame jedno slovo, co s nim? */

         strcat(blboutput, chword(word));
         strcat(blboutput, " ");
         if (string[poz] == ' ')
            poz++;
      }
   }

   if (string[strlen(string) - 1] == ' ')
      string[strlen(string) - 1] = '.';

   if (!strcmp(string, blboutput))
      switch (rand() % 14)
      {
      case 0:
         strcpy(blboutput, "Dnes je pekne, ze?");
         break;
      case 1:
         strcpy(blboutput, "Zajtra bude asi prsat...");
         break;
      case 2:
         strcpy(blboutput, "Co hovoris?");
         break;
      case 3:
         strcpy(blboutput, "Hm... to je na mna moc...");
         break;
      case 4:
         strcpy(blboutput, "Sorrry, nestiiiham....");
         break;
      case 5:
         strcpy(blboutput, "Mas f pazi...");
         break;
      case 6:
         strcpy(blboutput, "Skus to povedat inac, nechapem...");
         break;
      case 7:
         strcpy(blboutput, "Pravda je vo hviezdach...");
         break;
      case 8:
         strcpy(blboutput, "Neoblbuj ma.");
         break;
      case 9:
         strcpy(blboutput, "Clovece, ty asi nevies po slovensky!");
         break;
      case 10:
         strcpy(blboutput, "Pozri sa na http://www.atlantis.sk a mne daj pokoj.");
         break;
      case 11:
         strcpy(blboutput, "Veris v Boha?");
         break;
      case 12:
         strcpy(blboutput, "Tvoja veta je pre mna nezrozumitelna.");
         break;
      case 13:
         strcpy(blboutput, "Vies ty vobec, kto som ja?");
         break;
      }

   blboutput[0] = toupper(blboutput[0]);

   if (strstr(string, "!"))
   {
      string[0] = '\0';
      switch (rand() % 6)
      {
      case 0:
         strcat(string, "Nekric na mna...");
         break;
      case 1:
         strcat(string, "Neznervoznuj sa...");
         break;
      case 2:
         strcat(string, "Kluud, ticho lieci...");
         break;
      case 3:
         strcat(string, "Zvysovat hlas nemusis...");
         break;
      case 4:
         strcat(string, "Preco jacis???");
         break;
      case 5:
         strcat(string, "Zachovaj pokoj...");
         break;
      }
      strcat(string, blboutput);
      for (poz = 0; string[poz] != '\0'; poz++)
         blboutput[poz] = string[poz];
      blboutput[poz] = '\0';
   }

   if (strlen(blboutput) > 240)
      switch (rand() % 4)
      {
      case 0:
         strcpy(blboutput, "Prosim ta, nevies si na mna vymysliet nieco jednoduchsie???");
         break;
      case 1:
         strcpy(blboutput, "Pocuj, neprehanas to tak trosku???");
         break;
      case 2:
         strcpy(blboutput, "No tak toto je na mna FAKT moc!");
         break;
      case 3:
         strcpy(blboutput, "Teraz neviem, ci som taky blby, alebo je to take zlozite...");
         break;
      }
   return (blboutput);
}

/************** toto rozhoduje, ci bude tellovat alebo nieco robit **********/

char *nemo(instring) char instring[];
{
   char luzername[MAXWORD], repbuf[MAXSTRING];
   int poz;
   long repeat;

   nemoutput[0] = '\0';
   luzername[0] = '\0';

   strcpy(luzername, curword(instring, 1));

   if (!strcmp(curword(instring, 4), "~FKK") && ovlada(luzername))
   {
      if (!strcmp(curword(instring, 5), "*"))
         strcpy(instring, "~FKK .help");
      instring = strstr(instring, "~FKK ");
      for (poz = 5; instring[poz] != '\0'; poz++)
         nemoutput[poz - 5] = instring[poz];
      nemoutput[poz - 5] = '\0';

      /* normalne to vrati command.... ale co ak je to nieco ine ;-))? */

      if (!strcmp(curword(nemoutput, 1), ".travel"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
         {
            sendmud(".go nebesa");
            travel(curword(nemoutput, 2));
            sprintf(nemoutput, ".jail %s", curword(nemoutput, 2));
         }
         else
            sprintf(nemoutput, ".tell %s USING: .travel <luzername>", luzername);
      }

      else if (strstr(curword(nemoutput, 1), ".revwiz"))
         ;
      else if (strstr(curword(nemoutput, 1), ".rev"))
         sprintf(nemoutput, ".tell %s Sorry, to nepojde...", luzername);

      else if (!strcmp(curword(nemoutput, 1), ".forward"))
      {
         if (forward != -1 && !strcmp(masters[forward], luzername))
         {
            sprintf(nemoutput, ".tell %s Forward is ~LI~FROFF~RS~FW", luzername);
            forward = -1;
         }
         else
         {
            for (poz = 0; poz < MASTERS; poz++)
               if (!strcmp(luzername, masters[poz]))
                  break;

            if (forward != -1)
            {
               sprintf(nemoutput, ".tell %s Forward ~LI~FRSWITCHED~RS~FW by %s!", masters[forward], luzername);
               sendmud(nemoutput);
            }
            forward = poz;
            sprintf(nemoutput, ".tell %s Forward ~FG~LI~OLON~FW", masters[forward]);
         }
      }

      else if (!strcmp(curword(nemoutput, 1), ".alltopic"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
         {
            strcpy(repbuf, wordtoend(nemoutput, 2));

            for (poz = 0; poz < ROOMS; poz++)
            {
               sprintf(nemoutput, ".go %s", rooms[poz]);
               sendmud(nemoutput);
               sprintf(nemoutput, ".topic %s", repbuf);
               sendmud(nemoutput);
            }
            sprintf(nemoutput, ".tell %s Topic bol ~FGNASTAVENY~RS!", luzername);
         }
         else
            sprintf(nemoutput, ".tell %s USING: .alltopic <akytopic>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".repeat"))
      {
         if ((repeat = atol(curword(nemoutput, 2))) > 0)
         {
            sendmud(".go nebesa");
            strcpy(repbuf, "");
            for (poz = 3; strcmp(curword(nemoutput, poz), "*"); poz++)
            {
               strcat(repbuf, curword(nemoutput, poz));
               strcat(repbuf, " ");
            }
            opakuj(repbuf, repeat);
            sprintf(nemoutput, ".tell %s ~FTREPEAT:~FW Hotovo! :->", luzername);
         }
         else
            sprintf(nemoutput, ".tell %s USING: .repeat <kolkokrat> <akyprikaz>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".fmode"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
            formode = atoi(curword(nemoutput, 2));
         if (formode > 3 || formode < 0)
         {
            sprintf(nemoutput, ".tell %s USING: .fmode [N] ( N = <0;3>) [default mode 0]", luzername);
            formode = 0;
         }
         else
            sprintf(nemoutput, ".tell %s ~FTFORWARD~FW MODE SET TO: %d", luzername, formode);
      }

      else if (!strcmp(curword(nemoutput, 1), ".logmode"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
         {
            if (!strcmp(curword(nemoutput, 2), "cancel"))
            {
               sprintf(nemoutput, ".tell %s Maska pre logovanie bola zrusena...", luzername);
               strcpy(logmode, "");
            }
            else
            {
               strcpy(logmode, strlow(wordtoend(nemoutput, 2)));
               sprintf(nemoutput, ".tell %s Maska pre logovanie bola nastavena na \"%s\"...", luzername, logmode);
            }
         }
         else if (strlen(logmode))
            sprintf(nemoutput, ".tell %s Sucasna maska pre logovanie je: \"%s\"", luzername, logmode);
         else
            sprintf(nemoutput, ".tell %s Maska pre logovanie nebola zadana...", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".log"))
      {
         if (logging)
         {
            fprintf(logfile, "***** ZAZNAM VYPNUTY NA PRIKAZ: %s *****\n", luzername);
            fclose(logfile);
            logging = 0;
            sprintf(nemoutput, ".tell %s ~FT~OLLOGOVANIE JE ~LI~FRVYPNUTE~FW~RS", luzername);
         }
         else
         {
            if ((logfile = fopen(LOGFILE, "at")) == NULL)
               sprintf(nemoutput, ".tell %s ~FT~OLERROR: Nemozno otvorit logovaci subor!", luzername);
            else
            {
               logging++;
               sprintf(nemoutput, ".tell %s ~FT~OLLOGOVANIE JE ~LI~FGZAPNUTE~FW~RS", luzername);
               fprintf(logfile, "***** ZAZNAM ZAPNUTY NA PRIKAZ: %s *****\n", luzername);
            }
         }
      }

      else if (!strcmp(curword(nemoutput, 1), ".exitpos"))
      {
         if (!strcmp(curword(nemoutput, 2), "reboot"))
         {
            if (logging && forking)
               fprintf(logfile, "***** Reboot initialized by: %s\n", luzername);
            else if (forking == 0)
               fprintf(stderr, "***** Reboot initialized by: %s\n", luzername);
            quit_robot(0);
         }
         else if (!strcmp(curword(nemoutput, 2), "shutdown"))
         {
            if (logging && forking)
               fprintf(logfile, "***** Shutdown initialized by: %s\n", luzername);
            else if (forking == 0)
               fprintf(stderr, "***** Shutdown initialized by: %s\n", luzername);
            quit_robot(1);
         }
         else
            sprintf(nemoutput, ".tell %s USING: .exitpos [shutdown|reboot]", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".verzia"))
         sprintf(nemoutput, ".tell %s %s", luzername, VERSION);

      else if (!strcmp(curword(nemoutput, 1), ".prohibit"))
      {
         if (prohon)
         {
            sprintf(nemoutput, ".tell %s Prohibicia shoutov je ~FRVYPNUTA~RS", luzername);
            prohon = 0;
         }
         else
         {
            sprintf(nemoutput, ".tell %s Prohibicia shoutov je ~FGZAPNUTA~RS", luzername);
            prohon = 1;
         }
      }
      else if (!strcmp(curword(nemoutput, 1), ".zdrav"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
         {
            if (strcmp(curword(nemoutput, 2), "cancel"))
            {
               strcpy(zdrav, curword(nemoutput, 2));
               sprintf(nemoutput, ".tell %s Odteraz budem zdravit usera menom \"%s\"...", luzername, zdrav);
            }
            else
            {
               strcpy(zdrav, "");
               sprintf(nemoutput, ".tell %s OK, uz nebudem zdravit nikoho...", luzername);
            }
         }
         else
         {
            if (strlen(zdrav))
               sprintf(nemoutput, ".tell %s Momentalne zdravim usera menom \"%s\"...", luzername, zdrav);
            else
               sprintf(nemoutput, ".tell %s Momentalne nezdravim nikoho...", luzername);
         }
      }
      else if (!strcmp(curword(nemoutput, 1), ".zbavma"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
         {
            strcpy(repbuf, curword(nemoutput, 2)); /* meno "obete" do repbuf */
            switch (rand() % 7)
            {
            case 0:
               sprintf(nemoutput, ".bomb %s", repbuf);
               break;
            case 1:
               sprintf(nemoutput, ".jail %s", repbuf);
               break;
            case 2:
               sprintf(nemoutput, ".kill %s", repbuf);
               break;
            case 3:
               sprintf(nemoutput, ".exec %s .quit", repbuf);
               break;
            case 4:
               travel(repbuf);
               sprintf(nemoutput, ".jail %s", repbuf);
               break;
            case 5:
               sprintf(nemoutput, ".hug %s", repbuf);
               opakuj(nemoutput, 50);
               sprintf(nemoutput, ".tell %s Nabuduce .hug nahradim .kill!", repbuf);
               break;
            case 6:
               sprintf(nemoutput, ".bomb %s", repbuf);
               opakuj(nemoutput, 10);
               sprintf(nemoutput, ".tell %s Ako bolo v Sarajeve, Ferdinand?", repbuf);
               break;
            }
            sendmud(nemoutput);
            sprintf(nemoutput, ".tell %s User %s ~LI~FY~OLodpisany~RS~FW... ;-)", luzername, repbuf);
         }
         else
            sprintf(nemoutput, ".tell %s USING: .zbavma <luzername>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".help"))
         sprintf(nemoutput, ".tell %s %s", luzername, PRIKAZY);
   }

   /* okej, teraz par "privilegovanych" ludi a ich prikazy.... */
   else if (!strcmp(curword(instring, 4), "~o") && privil(luzername))
   {
      if (strlen(instring) > 160)
         strcpy(instring, "~o .help");
      if (!strcmp(curword(instring, 5), "*"))
         strcpy(instring, "~o .help");
      instring = strstr(instring, "~o ");
      for (poz = 3; instring[poz] != '\0'; poz++)
         nemoutput[poz - 3] = instring[poz];
      nemoutput[poz - 3] = '\0';

      if (!strcmp(curword(nemoutput, 1), ".move"))
      {
         if (strcmp(curword(nemoutput, 2), "*") && strcmp(curword(nemoutput, 3), "*"))
         {
            sprintf(instring, ".move %s ", curword(nemoutput, 2));
            strcat(instring, curword(nemoutput, 3));
            strcpy(nemoutput, instring);
         }
         else
            sprintf(nemoutput, ".tell %s USING: .move <user> <room>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".kill"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
            sprintf(nemoutput, ".kill %s", curword(nemoutput, 2));
         else
            sprintf(nemoutput, ".tell %s USING: .kill <luzer>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".unjail"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
            sprintf(nemoutput, ".jail %s cancel", curword(nemoutput, 2));
         else
            sprintf(nemoutput, ".tell %s USING: .unjail <luzer>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".jail"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
            sprintf(nemoutput, ".jail %s", curword(nemoutput, 2));
         else
            sprintf(nemoutput, ".tell %s USING: .jail <luzer>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".pictell"))
      {
         if (strcmp(curword(nemoutput, 2), "*") && strcmp(curword(nemoutput, 3), "*"))
         {
            sprintf(instring, ".pictell %s ", curword(nemoutput, 2));
            strcat(instring, curword(nemoutput, 3));
            strcpy(nemoutput, instring);
         }
         else
            sprintf(nemoutput, ".pictell %s USING: .pictell <user> <picture>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".bomb"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
            sprintf(nemoutput, ".bomb %s", curword(nemoutput, 2));
         else
            sprintf(nemoutput, ".tell %s USING: .bomb <luzer>", luzername);
      }

      else if (!strcmp(curword(nemoutput, 1), ".travel"))
      {
         if (strcmp(curword(nemoutput, 2), "*"))
         {
            travel(curword(nemoutput, 2));
            sprintf(nemoutput, ".jail %s", curword(nemoutput, 2));
         }
         else
            sprintf(nemoutput, ".tell %s USING: .travel <luzer>", luzername);
      }

      else
         sprintf(nemoutput, ".tell %s Mozne prikazy: .move <user> <kam>, .pictell <user> <obrazok>, .kill <user>, .travel <user>, .bomb <user>, .jail <user>, .unjail <user>", luzername);
   }

   /* Nieco robi: ........... */
   else if (strstr(instring, "promot") || strstr(instring, "povys"))
   {
      switch (rand() % 5)
      {
      case 1:
         strcpy(nemoutput, ".kill ");
         strcat(nemoutput, luzername);
         sendmud(".shout Toto berte ako vystrahu!!!");
         break;
      default:
         strcpy(nemoutput, ".tell ");
         strcat(nemoutput, luzername);
         strcat(nemoutput, " Nie, promote NEBUDE!!!");
         break;
      }
   }

   /* Telluje: ........... */
   else
   {
      instring = strstr(instring, ": ");

      for (poz = 2; instring[poz] != '\0'; poz++)
         instring[poz - 2] = instring[poz];
      instring[poz - 2] = '\0';

      strcpy(nemoutput, ".tell ");
      strcat(nemoutput, luzername);
      strcat(nemoutput, " ");
      strcat(nemoutput, blbec(instring, luzername));
   }

   return (nemoutput);
}

/************* Buko: strlow() - obrati vsetky pismena na male **********/
char *strlow(str) char str[];
{
   int n;
   lowoutput[0] = '\0';

   for (n = 0; n < strlen(str); n++)
   {
      lowoutput[n] = tolower(str[n]);
   }
   lowoutput[n] = '\0';
   return (lowoutput);
}

/************* Checkuje, ci ma dany user pravo Poseidona ovladat *********/
int ovlada(name) char *name;
{
   int n;
   for (n = 0; n < MASTERS; n++)
   {
      if (!strcmp(name, masters[n]))
         return 1;
   }
   return 0;
}

/************* Checkuje, ci ma dany user nejake privilegia **************/
int privil(name) char *name;
{
   int n;
   for (n = 0; n < PRIVILS; n++)
   {
      if (!strcmp(name, privils[n]))
         return 1;
   }
   return 0;
}

/************* Vezme prislusne slovo z vety *****************************/
char *curword(string, number) char string[];
int number;
{
   int m = 0, n = 0;

   while (--number)
   {
      while (isspace(string[n++]) && string[n] != '\0')
         continue;

      while (!isspace(string[n++]) && string[n] != '\0')
         continue;
   }

   while (string[n] != '\0' && !isspace(string[n]))
      cword[m++] = string[n++];

   cword[m] = '\0';
   if (strlen(cword))
      return (cword);
   return ("*");
}

/**************** Vezme vsetky slova od daneho do konca vety ************/
char *wordtoend(string, number) char string[];
int number;
{
   int m = 0, n = 0;

   while (--number)
   {
      while (isspace(string[n++]) && string[n] != '\0')
         continue;

      while (!isspace(string[n++]) && string[n] != '\0')
         continue;
   }

   while (string[n] != '\0')
      cword[m++] = string[n++];

   cword[m] = '\0';
   return (cword);
}

/************************ povozi luzera po atlantide :) *****************/
void travel(luzername) char luzername[];
{
   int poz;

   for (poz = 0; poz < ROOMS; poz++)
   {
      sprintf(nemoutput, ".move %s %s", luzername, rooms[poz]);
      sendmud(nemoutput);
      sendmud(".time");
   }
}

/*********************** Opakovane pusta do Atlantidy string **************/
void opakuj(string, repeat) char string[];
long repeat;
{
   while (repeat--)
   {
      sendmud(string);
      sendmud(".time"); /* Aby nesalel quoli floodu :-) */
   }
}
