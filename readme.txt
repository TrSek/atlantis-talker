Atlantis Talker v5.40
---------------------

Balik obsahuje kompletne prostredie pre spustenie Atlantisu na vlastnom
systeme, tzn. zdrojove texty + databazovy dump + pomocne subory.

Instalacia howto: V adresari src/ su zdrojaky - kompilacia klasickym
postupom (make), vygeneruje sa binarka atlantis.exe

Adresar sqldump obsahuje dump databazy potrebnej pre chod programu.
Pouziva sa MySQL, nazov databazy musi byt 'atlantis', pripadne po uprave
zdrojaku moze byt aj iny nazov. Login a heslo do databazy je v konfiguracnom
subore 'talker/atlantis.ini' - prvy riadok login, druhy heslo. Databazovy
server je 'localhost'.

Adresar talker/ - hlavny adresar, sem je treba nakopirovat binarku
atlantis.exe a spustit. Testovacie konto s plnymi pravami ("God")
je vytvorene, s nickom 'Slavko' a heslom: doremifa!

-- Zdrojove texty obsahuju vsetky povodne 'dristy' a poznamky autorov, preto
moze byt samotne citanie zdrojakov istou formou zabavy - ovsem v pripade ze
citatel pozna (niekedy dost zlozite) suvislosti :)

-- Verzia pravdepodobne este obsahuje nejake chyby, ak si trufate tak si to
upravte na svoj obraz :-) Kedze Atlantis Talker ukoncil svoju cinnost, v
jednej zo vzniknutych kopii (talker.sk 7000) boli odstranene niektore chyby
a talker bezi s niektorymi malymi upravami dalej.

-- Technicke info: Program by mal byt skompilovatelny na beznom Linuxe a
gcc, vyvijany bol pod roznymi verziami Slackware. MySQL server bol trojkovej
rady, avsak nemal by byt problem ani s verziou 4.x.

